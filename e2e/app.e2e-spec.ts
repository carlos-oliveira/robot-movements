import { RobotMovementsPage } from './app.po';

describe('robot-movements App', () => {
  let page: RobotMovementsPage;

  beforeEach(() => {
    page = new RobotMovementsPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Robot movements');
  });
});
