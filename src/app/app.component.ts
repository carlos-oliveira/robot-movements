import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Robot movements';

  facing: any = ['N', 'E', 'S', 'W'];

  x: number;
  y: number;
  f: string;
  currentPos : any = {};
  movements = [];
  showReport: boolean = false;

  getInitialPosition() {
    this.x = this.getRandom();
    this.y = this.getRandom();
    this.f = this.whereToFace(this.x, this.y);

    //Set the initial position
    this.resetCurrentPosition(this.x, this.y, this.f);

    this.auditMovements({'started at': this.currentPos});
  };

  moveMe() {
    if (this.f === '') {
      alert('Please turn left or right.');
    }
    //based on where this.f points to
    if (this.f === 'N') {
      //Y+1
      this.y = (this.y + 1);
    }

    if (this.f === 'S') {
      //Y -1
      this.y = (this.y - 1);
    }

    if (this.f === 'E') {
      //X+1
      this.x = (this.x + 1);
    }

    if (this.f === 'W') {
      //X-1
      this.x = (this.x - 1);
    }

    this.checkForExtremities();

    // reset the current position
    this.resetCurrentPosition(this.x, this.y, this.f);
    this.auditMovements({'Moved to': this.currentPos});

  };

  checkForExtremities(){
    if (this.x === 0 && this.f === 'W') {
        this.f = '';
    }

    if (this.x === 4 && this.f === 'E') {
      this.f = '';
    }
    if (this.y === 0 && this.f === 'S') {
      this.f = '';
    }

    if (this.y === 4 && this.f === 'N') {
      this.f = '';
    }

  }


  turnLeft() {
    //search the next pos on the array facing and set it to this.f
    if (this.f === '') {
      this.f = this.whereToFace(this.x, this.y);
    } else {

      let pos = this.facing.indexOf(this.f);

      if (pos == this.facing.length - 1) {
        this.f = this.facing[0];
      }

      if(pos >= 0 && pos < this.facing.length - 1){
        this.f = this.facing[pos + 1];
      }
    }

    // reset the current position
    this.resetCurrentPosition(this.x, this.y, this.f);

    this.auditMovements({'Turned Left': this.currentPos});
  };

  turnRight() {

    if (this.f === '') {
      this.f = this.whereToFace(this.x, this.y);
    } else {
      console.log(this.facing);
      let pos = this.facing.indexOf(this.f);
      console.log(pos);

      if (pos == 0) {
        this.f = this.facing[this.facing.length - 1];
      }
      //
      if(pos > 0){
        this.f = this.facing[pos - 1];
      }
    }
    //reset the current position
    this.resetCurrentPosition(this.x, this.y, this.f);

    this.auditMovements({'Turned Right': this.currentPos});

  };

  getReport() {
    this.showReport = true;

  };

  getRandom(){
    return Math.floor(Math.random() * 4) + 0;
  };

  whereToFace(numX, numY) {
    //cannot face west.
    if (numX === 0) {
      this.facing = ['N', 'E', 'S'];
    }
    //cannot face east
    if (numX === 4) {
      this.facing = ['N', 'S', 'W'];
    }

    //cannot face south
    if (numY === 0) {
      this.facing = ['N', 'E', 'W'];
    }

    //cannot face north
    if (numY === 4) {
      this.facing = ['E', 'S', 'W'];
    }

    //start block the 4 corners
    //cannot face west neither south
    if (numX === 0 && numY === 0) {
      this.facing = ['N', 'E'];
    }
    //cannot face north neither west
    if (numX === 0 && numY === 4) {
      this.facing = ['E', 'S'];
    }
    //cannot face north neither east
    if (numX === 4 && numY === 4) {
        this.facing = ['S', 'W'];
    }
    //cannot face east neither shout
    if (numX === 4 && numY === 0) {
        this.facing = ['W', 'N'];
    }
    //End block the 4 corners

    return this.facing[Math.floor(Math.random() * this.facing.length)];
  }

  resetCurrentPosition(x,y,f) {
    this.currentPos = {
      'x': x,
      'y': y,
      'f': f
    };
  }

  auditMovements(movement) {
    this.movements.push(JSON.stringify(movement));
  }
}
