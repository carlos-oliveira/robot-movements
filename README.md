# Robot Movements

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.0.0-rc.2.

## Get the project

Download or clone the project

## Install NodeJS

Install nodejs from the link:https://nodejs.org/en/download/

## Install Angular CLI

Install Angular CLI npm install -g @angular/cli`

## Install the project dependencies

On the folder of the project (robot-movements), run: `npm install`

## Running a server

Run `ng serve`. Open your browser and navigate to `http://localhost:4200/`.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).
Before running the tests make sure you are serving the app via `ng serve`.
